import React from "react";
import "./index.css";

interface State {
    checked: boolean;
}

interface Props {
    checked?: boolean;
    onChange: (checked: boolean) => void;
    disabled?: boolean;
}

export default class Switch extends React.PureComponent<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            checked: false,
        };
    }

    onClick = () => {
        this.props.onChange(!this.state.checked);
        this.setState({ checked: !this.state.checked });
    }

    render() {
        const switchClassList = "switch" + " " + (this.state.checked || this.props.checked ? "switch-checked" : "") + " " + (this.props.disabled ? "switch-disabled" : "");
        return <div className="comp-switch">
            <button className={switchClassList} onClick={typeof this.props.checked === "boolean" ? () => this.props.onChange(!this.props.checked) : this.onClick} disabled={this.props.disabled}>
                <div className="switch-inner" />
            </button>
        </div>;
    }
}
