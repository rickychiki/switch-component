import React from "react";
import ReactDOM from "react-dom";
import Switch from "./Switch";

interface State {
    isSwitchChecked: boolean;
    isSwitchDisabled: boolean;
    isUncontrolled: boolean;
}

class App extends React.PureComponent<{}, State> {
    public state: State = {
        isSwitchChecked: false,
        isSwitchDisabled: false,
        isUncontrolled: false,
    };

    onToggleDisable = () => this.setState({ isSwitchDisabled: !this.state.isSwitchDisabled });

    onChange = (checked: boolean) => this.setState({ isSwitchChecked: checked });

    onToggleUncontrolled = () => this.setState({ isUncontrolled: !this.state.isUncontrolled });

    render() {
        return (
            <div>
                <Switch checked={this.state.isUncontrolled ? undefined : this.state.isSwitchChecked} disabled={this.state.isSwitchDisabled} onChange={this.onChange} />
                <button type="button" onClick={this.onToggleDisable}>
                    Toggle Disable
                </button>
                <button type="button" onClick={this.onToggleUncontrolled}>
                    Toggle Uncontrol
                </button>
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById("app")!);
